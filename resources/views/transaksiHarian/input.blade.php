@extends('layouts.main')

@section('title')
    <div class="card p-3">
      <h3><a href="/transaksiHarian" class="btn btn-primary btn-sm"> Ganti Tanggal </a> Transaksi Harian Tanggal {{ $tanggal }}</h3>
    </div>
@endsection

@section('container')
    <div class="container">
        <div class="row mb-3">
            <!-- Earnings (Monthly) Card Example -->
            <div class="col-xl-3 col-md-6 mb-4">
              <div class="card h-90">
                <div class="card-body">
                  <div class="row align-items-center">
                    <div class="col mr-2">
                      <div class="text-xs font-weight-bold text-uppercase mb-1">JUMLAH TRANSAKSI</div>
                      <div class="h5 mb-0 font-weight-bold text-gray-800">{{ $jumlahTransaksi }}</div>
                    </div>
                    <div class="col-auto">
                      <i class="fa-solid fa-pen-to-square fa-2x text-primary"></i>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!-- Earnings (Annual) Card Example -->
            <div class="col-xl-3 col-md-6 mb-4">
              <div class="card h-90">
                <div class="card-body">
                  <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                      <div class="text-xs font-weight-bold text-uppercase mb-1">OMSET</div>
                      <div class="h5 mb-0 font-weight-bold text-gray-800"> @currency($totalPemasukan) </div>
                      
                    </div>
                    <div class="col-auto">
                      <i class="fa-solid fa-folder-plus fa-2x text-success"></i>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!-- New User Card Example -->
            <div class="col-xl-3 col-md-6 mb-4">
              <div class="card h-100">
                <div class="card-body">
                  <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                      <div class="text-xs font-weight-bold text-uppercase mb-1">PENGELUARAN</div>
                      <div class="h5 mb-0 mr-3 font-weight-bold text-gray-800">@currency($totalPengeluaran)</div>
                    </div>
                    <div class="col-auto">
                      <i class="fa-solid fa-folder-minus fa-2x text-danger"></i>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!-- Pending Requests Card Example -->
            <div class="col-xl-3 col-md-6 mb-4">
              <div class="card h-100">
                <div class="card-body">
                  <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                      <div class="text-xs font-weight-bold text-uppercase mb-1">PENDAPATAN BERSIH</div>
                      <div class="h5 mb-0 font-weight-bold text-gray-800">@currency($pendapatanBersih)</div>
                    </div>
                    <div class="col-auto">
                      <i class="fa-solid fa-scale-unbalanced-flip fa-2x text-warning"></i>
                    
                    </div>
                  </div>
                </div>
              </div>
            </div>

        </div>
        <div class="row">
            <div class="col-lg-6">
                <div class="card mb-4">
                    <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                      <h6 class="m-0 font-weight-bold text-primary">Input Transaksi Pendapatan</h6>
                    </div>
                    <div class="card-body">
                        <form action="/tambahPendapatan" method="post">
                            @csrf
                            <div class="form-group">
                                <input type="hidden" name="tanggal" value="{{ $tanggal }}">
                                <input type="hidden" name="keterangan" value="Pemasukan Harian">
                                <label for="exampleInputEmail1">Nominal</label>
                                <input type="text" class="form-control" name="nominal" autofocus>
                            </div>
                            <button type="submit" class="btn btn-primary">Tambah</button>
                        </form>
                    </div>
                  </div>
            </div>

            <div class="col-lg-6">
                <div class="card mb-4">
                    <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                      <h6 class="m-0 font-weight-bold text-primary">Input Transaksi Pengeluaran</h6>
                    </div>
                    <div class="card-body">
                      <form action="/tambahPengeluaran" method="post">
                        @csrf
                        <div class="form-group">
                          <input type="hidden" name="tanggal" value="{{ $tanggal }}">
                          <label for="exampleInputEmail1">Nominal</label>
                          <input type="text" class="form-control" name="nominal" >
                        </div>
                        <div class="form-group">
                          <label for="exampleInputEmail1">Keterangan</label>
                          <input type="text" class="form-control" name="keterangan">
                        </div>
                        <button type="submit" class="btn btn-primary">Tambah</button>
                      </form>
                    </div>
                  </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-6">
                <div class="table-responsive p-3">
                    <table class="table align-items-center table-flush" id="dataTable">
                      <thead class="thead-light">
                        <tr>
                          <th>No</th>
                          <th>Nominal</th>
                          <th>Keterangan</th>
                          <th>Aksi</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php 
                            $no = 1;
                        ?>
                        @forelse ($pemasukan as $item)
                        <tr>
                            <td>{{ $no++ }}</a></td>
                            <td><small>@currency($item->nominal)</small></td>
                            <td><small>{{ $item->keterangan }}</small></td>
                            <td><a href="/pemasukan/{{ $item->id }}" class="btn btn-warning btn-sm"><i class="bi bi-pencil-square"></i></a>
                                <form action="/hapusPemasukan/{{ $item->id }}" method="post" class="d-inline">
                                  @csrf
                                  @method('delete')
                                  <input type="hidden" name="tanggal" value="{{ $tanggal }}">
                                  <button type="submit" class="btn btn-danger btn-sm delete"><i class="bi bi-trash"></i></button>
                                </form></td>
                          </tr>
                        @empty
                            <tr>
                                <td colspan="4" class="text-center">Belum ada data transaksi masuk</td>
                            </tr>
                        @endforelse
                        
                      </tbody>
                    </table>
                  </div>
            </div>

            <div class="col-lg-6">
                <div class="table-responsive p-3">
                    <table class="table align-items-center table-flush" id="dataTableHover">
                      <thead class="thead-light">
                        <tr>
                          <th>No</th>
                          <th>Nominal</th>
                          <th>Keterangan</th>
                          <th>Aksi</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php 
                            $no = 1;
                        ?>
                        @forelse ($pengeluaran as $item)
                        <tr>
                            <td>{{ $no++ }}</a></td>
                            <td>@currency($item->nominal)</td>
                            <td>{{ $item->keterangan }}</td>
                            <td><a href="/pengeluaran/{{ $item->id }}" class="btn btn-warning btn-sm"><i class="bi bi-pencil-square"></i></a>
                                <form action="/hapusPengeluaran/{{ $item->id }}" method="post" class="d-inline">
                                  @csrf
                                  @method('delete')
                                  <input type="hidden" name="tanggal" value="{{ $tanggal }}">
                                  <button type="submit" class="btn btn-danger btn-sm delete"><i class="bi bi-trash"></i></button>
                                </form>
                            </td>
                          </tr>
                        @empty
                            <tr>
                              <td colspan="4" class="text-center">Belum ada data pengeluaran</td>
                            </tr>
                        @endforelse
                        
                      </tbody>
                    </table>
                  </div>
            </div>
        </div>
    </div>
@endsection