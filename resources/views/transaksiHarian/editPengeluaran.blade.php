@extends('layouts.main')

@section('title')
    
    <h3>Edit Pemasukan</h3>
@endsection

@section('container')
    <div class="container">
        <div class="row">
            <div class="col-lg-6">
                <div class="card mb-4">
                    <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                      <h6 class="m-0 font-weight-bold text-primary">Input Transaksi Pendapatan</h6>
                    </div>
                    <div class="card-body">
                        <form action="/updatePengeluaran/{{ $pengeluaran->id }}" method="post">
                            @csrf
                            @method('put')
                            <div class="form-group">
                                <input type="hidden" name="tanggal" value="{{ $pengeluaran->tanggal }}">
                                <label for="exampleInputEmail1">Nominal</label>
                                <input type="text" class="form-control" name="nominal" value="{{ $pengeluaran->nominal }}">
                            </div>
                            <div class="form-group">
                                <label for="exampleInputEmail1">Keterangan</label>
                                <textarea name="keterangan" class="form-control">{{ $pengeluaran->keterangan }}</textarea>
                            </div>
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </form>
                    </div>
                  </div>
            </div> 
        </div>
    </div>
@endsection