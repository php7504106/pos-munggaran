@extends('layouts.main')

@section('title')
    <h3>Transaksi Harian</h3>
@endsection

@section('container')
<div class="row">
    <div class="col-12 col-md-6 col-lg-6">
        <div class="card">
            <div class="card-header">
                <h4>Pilih Tanggal Transaksi<h4>
            </div>
            <div class="card-body">
            <form action="/viewTransaksi" method="GET">

            <div class="form-group" id="simple-date1">
                <label for="simpleDataInput">Pilih Tanggal Transaksi</label>
                <div class="input-group date">
                    <div class="input-group-prepend">
                        <span class="input-group-text"><i class="fas fa-calendar"></i></span>
                    </div>
                        <input type="text" class="form-control" name="pilihtanggal" id="simpleDataInput" placeholder="dd-mm-yyyy" required>
                </div>
                
            </div>

            <div class="form-group text-center" >
                <button type="submit" class="btn btn-primary btn-sm">Pilih</button>
            </div>

            </form>
        </div> 
    </div>
</div>
</div>
@endsection