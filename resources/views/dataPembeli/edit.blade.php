@extends('layouts.main')

@section('title')
    <h2>Edit Data Pembeli</h2>
@endsection

@section('container')
    <div class="container">
        <div class="row">
            <div class="col-lg-8">
                <div class="card">
                    <div class="card-body">
                        <form action="/prosesEditPembeli/{{ $data->id }}" method="post">
                            @csrf
                            @method('put')
                            <div class="form-group">
                                <label for="">Nama Pembeli</label>
                                <input type="hidden" name="hal" value="halamanMaster">
                                <input type="text" class="form-control" name="nama" value="{{ $data->nama }}" required>
                            </div>
                       
                            <div class="form-group">
                                <label for="">Nomor Telephone</label>
                                <input type="text" class="form-control" name="noTelp" value="{{ $data->noTelp }}" required>
                            </div>
                                <button type="submit" class="btn btn-primary ">Update</button>                            
                          
                        </form>
                    </div>
                </div>
                
            </div>
        </div>
    </div>
@endsection