@extends('layouts.main')

@section('title')
    <h2>Tambah Data Pembeli</h2>
@endsection

@section('container')
    <div class="container">
        <div class="row">
            <div class="col-lg-8">
                <div class="card">
                    <div class="card-body">
                        <form action="/prosesTambahPengutang" method="post">
                            @csrf
                            <div class="form-group">
                                <label for="">Nama Pembeli</label>
                                <input type="hidden" name="hal" value="halamanMaster">
                                <input type="text" class="form-control" name="nama" required>
                            </div>
                       
                            <div class="form-group">
                                <label for="">Nomor Telephone</label>
                                <input type="text" class="form-control" name="noTelp" required>
                            </div>
                                <button type="submit" class="btn btn-primary ">Tambah</button>                            
                          
                        </form>
                    </div>
                </div>
                
            </div>
        </div>
    </div>
@endsection