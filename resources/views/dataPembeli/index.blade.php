@extends('layouts.main')

@section('title')
    <div class="card p-3">
        <h2>Manajemen Data Pembeli</h2>
    </div>
@endsection

@section('container')
    <div class="container">
        <div class="card">
            <div class="card-body">
                <div class="row mb-3">
                    <div class="col-lg-12">
                        <a href="/pembeli/create" class="btn btn-primary btn-sm"><i class="bi bi-plus"></i> Tambah Data Pembeli</a>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="table-responsive p-3">
                            <table
                                class="table align-items-center table-flush table-hover"
                                id="dataTableHover"
                            >
                                <thead class="thead-light">
                                    <tr>
                                        <th>No</th>
                                        <th>Nama Pembeli</th>
                                        <th>Nomor Telephone</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $i=1; ?>
                                    @forelse ($dataPembeli as $pembeli)
                                    <tr>
                                        <td>{{ $i++ }}</td>
                                        <td>{{ $pembeli->nama }}</td>
                                        <td>{{ $pembeli->noTelp }}</td>
                                        <td>
                                            <a href="/editPembeli/{{ $pembeli->id }}" class="btn btn-warning btn-sm"><i class="bi bi-pencil-square"></i></a>
                                            <form action="/hapusPembeli/{{ $pembeli->id }}" method="post" class="d-inline">
                                            @csrf
                                            @method('delete')
                                            <button type="submit" class="btn btn-danger btn-sm delete"><i class="bi bi-trash"></i></button>
                                            </form>
                                        </td>
                                    </tr>
                                    @empty
                                        <tr>
                                            <td colspan="3">BELUM ADA DATA</td>
                                        </tr>
                                    @endforelse
                                    
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection