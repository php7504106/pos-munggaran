@extends('layouts.main')

@section('container')
    <div class="container">
        <div class="row">
            <div class="col-xl-3 col-md-6 mb-4">
                <div class="card h-90">
                <div class="card-body">
                    <div class="row align-items-center">
                    <div class="col mr-2">
                        <div class="text-xs font-weight-bold text-uppercase mb-1">JUMLAH TRANSAKSI HARI INI</div>
                        <div class="h5 mb-0 font-weight-bold text-gray-800">{{ $jumlahTransaksi }}</div>
                    </div>
                    <div class="col-auto">
                        <i class="fa-solid fa-pen-to-square fa-2x text-primary"></i>
                    </div>
                    </div>
                </div>
                </div>
            </div>
            <!-- Earnings (Annual) Card Example -->
            <div class="col-xl-3 col-md-6 mb-4">
                <div class="card h-100">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                        <div class="text-xs font-weight-bold text-uppercase mb-1">OMSET HARI INI</div>
                        <div class="h5 mb-0 font-weight-bold text-gray-800"> @currency($totalPemasukan) </div>
                        
                    </div>
                    <div class="col-auto">
                        <i class="fa-solid fa-folder-plus fa-2x text-success"></i>
                    </div>
                    </div>
                </div>
                </div>
            </div>
            <!-- New User Card Example -->
            <div class="col-xl-3 col-md-6 mb-4">
                <div class="card h-100">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                        <div class="text-xs font-weight-bold text-uppercase mb-1">PENGELUARAN HARI INI</div>
                        <div class="h5 mb-0 mr-3 font-weight-bold text-gray-800">@currency($totalPengeluaran)</div>
                    </div>
                    <div class="col-auto">
                        <i class="fa-solid fa-folder-minus fa-2x text-danger"></i>
                    </div>
                    </div>
                </div>
                </div>
            </div>
            <!-- Pending Requests Card Example -->
            <div class="col-xl-3 col-md-6 mb-4">
                <div class="card h-100">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                        <div class="text-xs font-weight-bold text-uppercase mb-1">PENDAPATAN BERSIH HARI INI</div>
                        <div class="h5 mb-0 font-weight-bold text-gray-800">@currency($pendapatanBersih)</div>
                    </div>
                    <div class="col-auto">
                        <i class="fa-solid fa-scale-unbalanced-flip fa-2x text-warning"></i>
                    
                    </div>
                    </div>
                </div>
                </div>
            </div>
        </div>
    </div>
@endsection