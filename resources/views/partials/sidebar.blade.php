<ul class="navbar-nav sidebar sidebar-light accordion" id="accordionSidebar">
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="/">
      <div class="sidebar-brand-text mx-3">PoS RM Munggaran</div>
    </a>
    <hr class="sidebar-divider my-0" />
    <li class="nav-item">
      <a class="nav-link" href="/dashboard">
        <i class="bi bi-house"></i>
        <span>Dashboard</span></a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="/transaksiHarian">
          <i class="bi bi-question-circle"></i>
        <span>Transaksi Harian</span></a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="/questions">
          <i class="bi bi-question-circle"></i>
        <span>Transaksi Cathering</span></a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="/category">
          <i class="bi bi-tags"></i>
        <span>Laporan Keuangan</span></a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="/hutang">
          <i class="bi bi-tags"></i>
        <span>Manajemen Hutang</span></a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="/category">
          <i class="bi bi-tags"></i>
        <span>Absensi Karyawan</span></a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="/dataPembeli">
          <i class="bi bi-tags"></i>
        <span>Data Pembeli</span></a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="/menu">
          <i class="bi bi-tags"></i>
        <span>Data Menu</span></a>
    </li>
</ul>