@extends('layouts.main')

@section('title')
    <h2>Pembayaran Tagihan/Hutang</h2>
@endsection

@section('container')
    <div class="container">
        <div class="row">
            <div class="col-lg-8">
                <div class="card">
                    <div class="card-body">
                        <table width="100%">
                            <tr>
                                <td>Nama</td> 
                                <td>:</td>
                                <td>{{ $dataHutang->pembeli->nama }}</td>
                            </tr>
                            <tr>
                                <td>Nomor Telephone</td> 
                                <td>:</td>
                                <td>{{ $dataHutang->pembeli->noTelp }}</td>
                            </tr>
                            <tr>
                                <td>Jatuh Tempo</td> 
                                <td>:</td>
                                <td>{{ $dataHutang->jatuh_tempo }}</td>
                            </tr>
                            <tr>
                                <td>Total Hutang</td> 
                                <td>:</td>
                                <td>@currency($dataHutang->total_hutang)</td>
                            </tr>
                            <tr>
                                <td>Sisa Bayar</td> 
                                <td>:</td>
                                <td>@currency($dataHutang->total_hutang - $dataHutang->total_bayar)</td>
                            </tr>
                            <form action="/prosesBayar/{{ $dataHutang->id }}" method="post">
                                @csrf
                            <tr>
                                <td><label for="simpleDataInput">Tanggal Bayar</label></td>
                                <td>:</td>
                                <td>
                                    <div class="form-group" id="simple-date1">
                                        <div class="input-group date">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text"><i class="fas fa-calendar"></i></span>
                                            </div>
                                                <input type="text" class="form-control" name="tanggal" id="simpleDataInput" placeholder="dd-mm-yyyy" required>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <div class="form-group d-block inline">
                                    <td><label for="">Masukan Jumlah Bayar</label></td>
                                    <td>:</td>
                                    <td><input type="text" class="form-control" name="total_bayar" required></td>
                                </div>
                            </tr>
                            <tr>
                                <td colspan="3" class="text-center">
                                    <div class="form-group mt-3">
                                        <button type="submit" class="btn btn-primary btn-sm">Bayar</button>
                                    </div>
                                </td>
                            </tr>
                        </form>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection