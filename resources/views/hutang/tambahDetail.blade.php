@extends('layouts.main')

@section('title')
    <h2>Tambah Data Hutang/Tagihan</h2>
@endsection

@section('container')
    <div class="container">
        <div class="row">
            <div class="col-lg-8">
                <div class="card">
                    <div class="card-body">
                        <form action="/prosesTambahDetail" method="post">
                            @csrf
                            <div class="form-group " id="simple-date1">
                                <input type="hidden" name="hutang_id" value="{{ $idHutang }}">
                                <label for="simpleDataInput">Pilih Tanggal</label>
                                <div class="input-group date">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="fas fa-calendar"></i></span>
                                    </div>
                                    <input type="text" class="form-control" name="tanggal" id="simpleDataInput" placeholder="dd-mm-yyyy" required>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="">Nominal</label>
                                <input type="text" class="form-control" name="nominal" required>
                            </div>
                            <div class="form-group">
                                <label for="">Qty</label>
                                <input type="number" class="form-control" name="qty" placeholder="0" required>
                            </div>
                            <div class="form-group">
                                <label for="">Keterangan</label>
                                <input type="text" class="form-control" name="keterangan"  required>
                            </div>
                            <button type="submit" class="btn btn-primary btn-sm">Tambah</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection