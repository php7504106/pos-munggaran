@extends('layouts.main')

@section('title')
    <h2>Tambah Hutang</h2>
@endsection

@section('container')
    <div class="container">
        <div class="row">
            <div class="col-lg-8">
                <div class="card">
                    <div class="card-body">
                        <form action="/hutang" method="post">
                            @csrf
                            <a href="/tambahDataPengutang" class="btn btn-primary btn-sm"><i class="bi bi-plus"></i> Buat Data Pengutang</a>
                            <div class="form-group mt-3">
                                <label for="">Pilih Nama Pengutang</label>
                                <select name="pembeli_id" class="form-control mb-3">
                                    @forelse ($pembeli as $item)
                                    <option value="{{ $item->id }}">{{ $item->nama }}</option>
                                    @empty
                                    <option>Belum Ada Data Pengutang</option>
                                    @endforelse
                                </select>
                            </div>
                            <div class="form-group" id="simple-date1">
                                <label for="simpleDataInput">Pilih Tanggal Jatuh Tempo</label>
                                <div class="input-group date">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text"><i class="fas fa-calendar"></i></span>
                                    </div>
                                        <input type="text" class="form-control" name="jatuh_tempo" id="simpleDataInput" placeholder="dd-mm-yyyy" required>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-primary">Tambah</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection