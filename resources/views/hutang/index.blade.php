@extends('layouts.main')

@section('title')
    <h2>Manajemen Tagihan/Hutang</h2>
@endsection

@section('container')
    <div class="container">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-lg-12">
                    <a href="/hutang/create" class="btn btn-primary btn-sm"><i class="bi bi-plus"></i> Buat Tagihan/Hutang</a>
                    </div>
                </div> 
                <div class="row">
                    <div class="table-responsive p-3">
                        <table
                            class="table align-items-center table-flush"
                            id="dataTable">
                            <thead class="thead-light">
                                <tr>
                                    <th>Nama</th>
                                    <th>Total Hutang</th>
                                    <th>Total Bayar</th>
                                    <th>Sisa Bayar</th>
                                    <th>Status</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse ($dataHutang as $hutang)
                                <tr>
                                    <td><small>{{ $hutang->pembeli->nama }}</small></td>
                                    <td><small>@currency($hutang->total_hutang)</small></td>
                                    @if ($hutang->total_bayar == 0)
                                    <td><small><span class="badge badge-danger">Belum Bayar</span></small></td>
                                    @else
                                        <td><small>@currency($hutang->total_bayar)</small></td>
                                    @endif
                                    <td><small>@currency($hutang->total_hutang - $hutang->total_bayar)</small></td>
                                    @if ($hutang->status == "Belum Lunas")
                                        <td><small><span class="badge badge-danger">{{ $hutang->status }}</span></small></td>
                                    @else
                                        <td><small><span class="badge badge-success">{{ $hutang->status }}</span></small></td>
                                    @endif
                                    <td>
                                        @if ($hutang->status == "Belum Lunas")
                                        <a href="/bayar/{{ $hutang->id }}" class="btn btn-primary btn-sm">Bayar</a>
                                        <form action="/tambahDetail" method="get" class="d-inline">
                                            <input type="hidden" name="idHutang" value="{{ $hutang->id }}">
                                            <button type="submit" class="btn btn-success btn-sm"><i class="bi bi-plus" data-toggle="tooltip" data-placement="top" title="Tambah Rincian"></i></button>
                                        </form>
                                        
                                        <a href="/invoice/{{ $hutang->id }}" target="_blank" class="btn btn-secondary btn-sm"><i class="bi bi-eye"></i></a>
                                        <a href="/invoice/{{ $hutang->id }}/download" class="btn btn-warning btn-sm"><i class="bi bi-download"></i></a>
                                        @else
                                        <a href="/invoice/{{ $hutang->id }}" target="_blank" class="btn btn-secondary btn-sm"><i class="bi bi-eye"></i></a>
                                        <a href="/invoice/{{ $hutang->id }}/download" class="btn btn-warning btn-sm"><i class="bi bi-download"></i></a>
                                        @endif
                                        
                                    </td>
                                </tr>
                                @empty
                                    <td>Belum ada data</td>
                                @endforelse
                            </tbody>
                        </table>
                    </div>
                </div> 
            </div>
        </div>
    </div>
    <script>
        $(function () {
            $('[data-toggle="tooltip"]').tooltip()
        })
    </script>
@endsection