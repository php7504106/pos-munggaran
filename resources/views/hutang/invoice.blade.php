<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>Invoice {{ $hutang->id }}</title>

    <style>
        html,
        body {
            margin: 10px;
            padding: 10px;
            font-family: sans-serif;
        }
        h1,h2,h3,h4,h5,h6,p,span,label {
            font-family: sans-serif;
        }
        table {
            width: 100%;
            border-collapse: collapse;
            margin-bottom: 0px !important;
        }
        table thead th {
            height: 28px;
            text-align: left;
            font-size: 16px;
            font-family: sans-serif;
        }
        table, th, td {
            border: 1px solid #ddd;
            padding: 8px;
            font-size: 14px;
        }

        .heading {
            font-size: 24px;
            margin-top: 12px;
            margin-bottom: 12px;
            font-family: sans-serif;
        }
        .small-heading {
            font-size: 18px;
            font-family: sans-serif;
        }
        .total-heading {
            font-size: 18px;
            font-weight: 700;
            font-family: sans-serif;
        }
        .order-details tbody tr td:nth-child(1) {
            width: 20%;
        }
        .order-details tbody tr td:nth-child(3) {
            width: 20%;
        }

        .text-start {
            text-align: left;
        }
        .text-end {
            text-align: right;
        }
        .text-center {
            text-align: center;
        }
        .company-data span {
            margin-bottom: 4px;
            display: inline-block;
            font-family: sans-serif;
            font-size: 14px;
            font-weight: 400;
        }
        .no-border {
            border: 1px solid #fff !important;
        }
        .bg-blue {
            background-color: #414ab1;
            color: #fff;
        }
    </style>
</head>
<body>

    <table class="order-details">
        <thead>
            <tr>
                <th width="50%" colspan="2">
                    <h2 class="text-start">Rumah Makan Munggaran</h2>
                </th>
                <th width="50%" colspan="2" class="text-end company-data">
                    <span>Id Invoice: {{ $hutang->id }}</span> <br>
                    <span>Tanggal: {{ $todayDate }}</span> <br>
                    <span>Alamat: Jl H. Darham Kp. Cikopo RT01/03 Babakan Peuteuy Cicalengka, 40395 </span> <br>
                </th>
            </tr>
            <tr class="bg-blue">
                <th width="50%" colspan="2">Detail Order</th>
                <th width="50%" colspan="2">Detail Pembeli</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>Order Id:</td>
                <td>{{ $hutang->id }}</td>

                <td>Nama/Instansi:</td>
                <td>{{ $hutang->pembeli->nama }}</td>
            </tr>
            <tr>
                <td>Tanggal Order:</td>
                <td>{{ $todayDate }}</td>

                <td>Nomor Telophone:</td>
                <td>{{ $hutang->pembeli->noTelp }}</td>
            </tr>
            <tr>
                <td>Jenis Pembayaran:</td>
                <td>Tunai</td>

            </tr>
            <tr>
                <td>Status Order:</td>
                <td>{{ $hutang->status }}</td>
            </tr>
        </tbody>
    </table>

    <table>
        <thead>
            <tr>
                <th class="no-border text-start heading" colspan="5">
                    Order Items
                </th>
            </tr>
            <tr class="bg-blue">
                <th>Tanggal</th>
                <th>Harga</th>
                <th>Qty</th>
                <th>Keterangan</th>
                <th>Subtotal</th>
            </tr>
        </thead>
        <tbody>
            @forelse ($detailHutang as $item)
            <tr>
                <td width="15%">{{ Carbon\Carbon::parse($item->tanggal)->format('d/m/Y') }}
                </td>
                <td width="15%">
                    @currency($item->nominal)
                </td>
                <td width="10%">{{ $item->qty }}</td>
                <td >{{ $item->keterangan }}</td>
                <td width="20%" class="fw-bold">@currency($item->subtotal)</td>
            </tr>
            @empty
                
            @endforelse
            
            <tr>
                <td colspan="4" class="total-heading">Jumlah Total:</td>
                <td colspan="1" class="total-heading">@currency($totalHutang)</td>
            </tr>
        </tbody>
    </table>

    <br>
    <p class="text-center">
        Terima Kasih Telah Berbelanja di Rumah Makan Munggaran
    </p>

</body>
</html>