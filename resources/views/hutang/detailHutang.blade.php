@extends('layouts.main')

@section('title')
    <h2> <a href="/hutang" class="btn btn-primary btn-sm"><i class="bi bi-backspace"></i> Kembali</a> Daftar Hutang dari {{ $hutang->pembeli->nama }}</h2>
@endsection

@section('container')
    <div class="container">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-lg-12">
                        <a href="/tambahDetailHutang/{{ $idHutang }}" class="btn btn-primary btn-sm"><i class="bi bi-plus"></i> Tambah Hutang/Tagihan</a>
                    </div>
                </div>
                <div class="row">
                    <div class="table-responsive p-3">
                        <table
                            class="table align-items-center table-flush"
                            id="dataTable">
                            <thead class="thead-light">
                                <tr>
                                    <th>Tanggal</th>
                                    <th>Nominal</th>
                                    <th>Qty</th>
                                    <th>Subtotal</th>
                                    <th>Keterangan</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse ($detailHutang as $item)
                                    <tr>
                                        <td>{{ date('d/m/Y',strtotime($item->tanggal)); }}
                                        </td>
                                        <td>@currency($item->nominal)</td>
                                        <td>{{ $item->qty }}</td>
                                        <td>@currency($item->subtotal)</td>
                                        <td>{{ $item->keterangan }}</td>
                                        <td><a href="/editDetailHutang/{{ $item->id }}" class="btn btn-warning btn-sm">Edit</a>
                                            <form action="/hapusDetailHutang/{{ $item->id }}" method="post" class="d-inline">
                                              @csrf
                                              @method('delete')
                                              <input type="hidden" name="hutang_id" value="{{ $item->hutang_id }}">
                                              <button type="submit" class="btn btn-danger btn-sm delete">Hapus</button>
                                            </form>
                                        </td>
                                    </tr>
                                @empty
                                    
                                @endforelse
                                <tr class="table-active">
                                    <td colspan="5" class="text-right font-weight-bold">Total Tagihan/Hutang :</td>
                                    <td class=" font-weight-bold">@currency($totalHutang)</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection