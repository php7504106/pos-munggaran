@extends('layouts.main')

@section('title')
    <div class="card p-3">
        <h2>Manajemen Data Menu</h2>
    </div>
@endsection

@section('container')
    <div class="container">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-lg-12">
                        <a href="/menu/create" class="btn btn-primary btn-sm"><i class="bi bi-plus"></i> Tambah Menu</a>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="table-responsive p-3">
                            <table
                                class="table align-items-center table-flush table-hover"
                                id="dataTableHover"
                            >
                                <thead class="thead-light">
                                    <tr>
                                        <th>No</th>
                                        <th>Nama Menu</th>
                                        <th width="250px">Deskripsi</th>
                                        <th>Harga</th>
                                        <th>Aksi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $i=1; ?>
                                    @forelse ($data as $item)
                                    <tr>
                                        <td>{{ $i++ }}</td>
                                        <td>{{ $item->nama }}</td>
                                        <td>{{ $item->deskripsi }}</td>
                                        <td>@currency($item->harga)</td>
                                        <td>
                                            <div class="d-inline">
                                                <a href="/menu/{{ $item->id }}/edit" class="btn btn-warning btn-sm"><i class="bi bi-pencil-square"></i></a>
                                            </div>
                                            <div class="d-inline">
                                                <form action="/menu/{{ $item->id }}" method="post" class="d-inline">
                                                    @csrf
                                                    @method('delete')
                                                    <button type="submit" class="btn btn-danger btn-sm delete"><i class="bi bi-trash"></i></button>
                                                    </form>
                                            </div>
                                        </td>
                                    </tr>
                                    @empty
                                        <tr>
                                            <td colspan="5">BELUM ADA DATA</td>
                                        </tr>
                                    @endforelse
                                    
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection