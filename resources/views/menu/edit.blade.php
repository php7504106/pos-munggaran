@extends('layouts.main')

@section('title')
    <div class="card p-3">
        <div class="d-flex justify-content-between">
            <div>
                <h2>Edit Data Menu</h2>
            </div>
            <div class="">
                <h5>
                    <a href="/dashboard" class="text-decoration-none">Home</a>/<a href="/menu" class="text-decoration-none">Manajemen Menu</a>/
                </h5>
            </div>
        </div>
    </div>
@endsection

@section('container')
    <div class="container">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-lg-12">
                        <form action="/menu/{{ $data->id }}" method="post">
                        @csrf
                        @method('put')
                        <div class="form-group">
                            <label for="">Nama Menu</label>
                            <input type="text" class="form-control" value="{{ $data->nama }}" name="nama" required>
                        </div>
                        <div class="form-group">
                            <label for="">Deskripsi</label>
                            <input type="text" class="form-control" value="{{ $data->deskripsi }}" name="deskripsi" required>
                        </div>
                        <div class="form-group">
                            <label for="">Harga</label>
                            <input type="text" class="form-control" value="{{ $data->harga }}" name="harga" required>
                        </div>
                        <div class="form-group text-center">
                            <button type="submit" class="btn btn-primary">Update</button>
                        </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection