<?php

namespace App\Http\Controllers;

use App\Models\DetailHutang;
use App\Models\Hutang;
use Illuminate\Http\Request;
use Illuminate\Queue\RedisQueue;
use RealRashid\SweetAlert\Facades\Alert;
use Illuminate\Support\Facades\DB;

class DetailHutangController extends Controller
{
    public function index(Request $request)
    {
        $hutang = Hutang::where('id', $request['idHutang'])->first();
        
        $detailHutang = DetailHutang::where('hutang_id', $request['idHutang'])->get();
        $totalHutang = $detailHutang->sum('subtotal');
        return view('hutang.detailHutang', [
            'hutang' => $hutang,
            'detailHutang' => $detailHutang,
            'idHutang' => $request->idHutang,
            'totalHutang' => $totalHutang

        ]);
    }

    public function create($id) {
        return view('hutang.tambahDetail', [
            'idHutang' => $id
        ]);
    }

    public function store(Request $request)
    {
        $dataHutang = new DetailHutang();
        $dataHutang->tanggal = date('Y/m/d',strtotime($request['tanggal']));
        $dataHutang->nominal = $request['nominal'];
        $dataHutang->qty = $request['qty'];
        $dataHutang->subtotal = $request['qty'] * $request['nominal'];
        $dataHutang->keterangan = $request['keterangan'];
        $dataHutang->hutang_id = $request['hutang_id'];
        $dataHutang->save();

        $totalHutang = Hutang::find($request['hutang_id']);
        $total = DetailHutang::where('hutang_id', $request['hutang_id']);
        $totalHutang->total_hutang = $total->sum('subtotal');
        if(($totalHutang->total_hutang - $totalHutang->total_bayar) > 0) {
            $totalHutang->status = 'Belum Lunas';
        } 
        
        $totalHutang->save();

        toast('Data berhasil ditambahkan','success');
        return redirect('/tambahDetail?idHutang='. $request['hutang_id']);
    }

    public function edit($id)
    {
        $detailHutang = DetailHutang::find($id);
        $oldTanggal = date('d-m-Y',strtotime($detailHutang->tanggal));
        return view('hutang.editDetailHutang', [
            'dataDetailHutang' => $detailHutang,
            'oldTanggal' => $oldTanggal
        ]);
    }

    public function update(Request $request, $id)
    {
        $dataHutang = DetailHutang::find($id);
        $dataHutang->tanggal = date('Y/m/d',strtotime($request['tanggal']));
        $dataHutang->nominal = $request['nominal'];
        $dataHutang->qty = $request['qty'];
        $dataHutang->subtotal = $request['qty'] * $request['nominal'];
        $dataHutang->keterangan = $request['keterangan'];
        $dataHutang->hutang_id = $request['hutang_id'];
        $dataHutang->save();

        $totalHutang = Hutang::find($request['hutang_id']);
        $total = DetailHutang::where('hutang_id', $request['hutang_id']);
        $totalHutang->total_hutang = $total->sum('subtotal');
        if(($totalHutang->total_hutang - $totalHutang->total_bayar) > 0) {
            $totalHutang->status = 'Belum Lunas';
        } 
        $totalHutang->save();

        toast('Data berhasil ditambahkan','success');
        return redirect('/tambahDetail?idHutang='. $request['hutang_id']);
    }

    public function destroy(Request $request, $id) 
    {
        $hutang_id = $request['hutang_id'];
        DB::table('detail_hutang')->where('id', '=', $id)->delete();

        $totalHutang = Hutang::find($request['hutang_id']);
        $total = DetailHutang::where('hutang_id', $request['hutang_id']);
        $totalHutang->total_hutang = $total->sum('subtotal');
        if(($totalHutang->total_hutang - $totalHutang->total_bayar) > 0) {
            $totalHutang->status = 'Belum Lunas';
        } else {
            $totalHutang->status = 'Lunas';
        }
        $totalHutang->save();

        toast('Data berhasil dihapus','success');
        return redirect('/tambahDetail?idHutang=' . $hutang_id);
    }
}
