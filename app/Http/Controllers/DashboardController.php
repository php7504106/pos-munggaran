<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Models\Pemasukan;
use Illuminate\Support\Facades\DB;
use App\Models\Pengeluaran;

class DashboardController extends Controller
{
    public function index() {
        $currentDate = Carbon::now()->format('Y-m-d');
        $dataPemasukan = Pemasukan::where('tanggal', $currentDate)->get();
        $dataPengeluaran = Pengeluaran::where('tanggal', $currentDate)->get();
        $jumlahTransaksi = count($dataPemasukan);
        $totalPemasukan = $dataPemasukan->sum('nominal');
        $totalPengeluaran = $dataPengeluaran->sum('nominal');
        $pendaparanBersih = $totalPemasukan - $totalPengeluaran;
        
        return view('dashboard', [
            'jumlahTransaksi' => $jumlahTransaksi,
            'totalPemasukan' => $totalPemasukan,
            'totalPengeluaran' => $totalPengeluaran,
            'pendapatanBersih' => $pendaparanBersih
        ]);
    }
}
