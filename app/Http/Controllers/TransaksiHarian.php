<?php

namespace App\Http\Controllers;

use App\Models\Pemasukan;
use App\Models\Pengeluaran;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Symfony\Component\Console\Command\DumpCompletionCommand;
use RealRashid\SweetAlert\Facades\Alert;

class TransaksiHarian extends Controller
{
    public function index(Request $request){
        $tanggal = date('Y/m/d',strtotime($request['pilihtanggal']));
        $pemasukan = Pemasukan::where('tanggal', $tanggal)->latest()->get();
        $totalPemasukan = $pemasukan->sum('nominal');
        $pengeluaran = Pengeluaran::where('tanggal', $tanggal)->latest()->get();
        $totalPengeluaran = $pengeluaran->sum('nominal');
        $jumlahPemasukan = count($pemasukan);
        $pendapatanBersih = $totalPemasukan - $totalPengeluaran;

        return view('transaksiHarian.input', [
            'tanggal' => $request['pilihtanggal'],
            'pemasukan' => $pemasukan,
            'pengeluaran' => $pengeluaran,
            'jumlahTransaksi' => $jumlahPemasukan,
            'totalPemasukan' => $totalPemasukan,
            'totalPengeluaran' => $totalPengeluaran,
            'pendapatanBersih' => $pendapatanBersih
        ]);
    }

    public function tambahPendapatan(Request $request) {
        $pemasukan = new Pemasukan();
        $pemasukan->nominal = $request->nominal;
        $pemasukan->keterangan = $request->keterangan;
        $pemasukan->tanggal = date('Y/m/d',strtotime($request->tanggal));
        $pemasukan->user_id = auth()->user()->id;

        $pemasukan->save();
        toast('Data berhasil ditambahkan','success');
        return redirect('/viewTransaksi?pilihtanggal=' . $request->tanggal);
    }
    public function tambahPengeluaran(Request $request) {
        $pengeluaran = new Pengeluaran();
        $pengeluaran->nominal = $request->nominal;
        $pengeluaran->keterangan = $request->keterangan;
        $pengeluaran->tanggal = date('Y/m/d',strtotime($request->tanggal));
        $pengeluaran->user_id = auth()->user()->id;

        $pengeluaran->save();
        toast('Data berhasil ditambahkan','success');
        return redirect('/viewTransaksi?pilihtanggal=' . $request->tanggal);
    }

    public function editPemasukan($id) {
        $pemasukan = Pemasukan::where('id', $id)->first();

        return view('transaksiHarian.editPemasukan', [
            'pemasukan' => $pemasukan
        ]);
    }
    public function editPengeluaran($id) {
        $pengeluaran = Pengeluaran::where('id', $id)->first();

        return view('transaksiHarian.editPengeluaran', [
            'pengeluaran' => $pengeluaran
        ]);
    }

    public function updatePendapatan(Request $request, $id)
    {
        $pemasukan = Pemasukan::find($id);

        $pemasukan->nominal = $request->nominal;
        $pemasukan->save();
        toast('Data berhasil diedit','success');
        return redirect('/viewTransaksi?pilihtanggal='. $pemasukan->tanggal);
    }
    public function updatePengeluaran(Request $request, $id)
    {
        $pengeluaran = Pengeluaran::find($id);

        $pengeluaran->nominal = $request->nominal;
        $pengeluaran->keterangan = $request->keterangan;
        $pengeluaran->save();
        toast('Data berhasil diedit','success');
        return redirect('/viewTransaksi?pilihtanggal='. $pengeluaran->tanggal);
    }

    public function destroyPemasukan(Request $request, $id)
    {
        $tanggal = $request['tanggal'];
        DB::table('pemasukan')->where('id', '=', $id)->delete();
        toast('Data berhasil dihapus','success');
        return redirect('/viewTransaksi?pilihtanggal=' . $tanggal);
    }
    public function destroyPengeluaran(Request $request, $id)
    {
        $tanggal = $request['tanggal'];
        DB::table('pengeluaran')->where('id', '=', $id)->delete();
        toast('Data berhasil dihapus','success');
        return redirect('/viewTransaksi?pilihtanggal=' . $tanggal);
    }
}
