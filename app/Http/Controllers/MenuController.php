<?php

namespace App\Http\Controllers;

use App\Models\Menu;
use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;

class MenuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Menu::latest()->get();
        return view('menu.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('menu.tambah');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $dataMenu = new Menu();
        $dataMenu->nama = $request['nama'];
        $dataMenu->deskripsi = $request['deskripsi'];
        $dataMenu->harga = $request['harga'];
        $dataMenu->save();

        toast('Data berhasil ditambahkan','success');
        return redirect('/menu');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data = Menu::find($id);
        
        return view('menu.edit', compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $dataMenu = Menu::find($id);
        $dataMenu->nama = $request['nama'];
        $dataMenu->deskripsi = $request['deskripsi'];
        $dataMenu->harga = $request['harga'];
        $dataMenu->save();

        toast('Data berhasil diedit','success');
        return redirect('/menu');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $dataMenu = Menu::find($id);
        $dataMenu->delete();

        toast('Data berhasil dihapus','success');
        return redirect('/menu');
    }
}
