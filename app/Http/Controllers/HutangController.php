<?php

namespace App\Http\Controllers;

use App\Models\DetailHutang;
use App\Models\Hutang;
use App\Models\Pemasukan;
use App\Models\Pembeli;
use Barryvdh\DomPDF\Facade\Pdf;
use Carbon\Carbon;
use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;

class HutangController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $dataHutang = Hutang::latest()->get();
        
        return view('hutang.index', [
            'dataHutang' => $dataHutang
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $pembeli = Pembeli::all();
        return view('hutang.tambah', [
            'pembeli' => $pembeli
        ]);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $hutang = new Hutang;
        $hutang->jatuh_tempo = date('Y/m/d',strtotime($request['jatuh_tempo']));
        $hutang->pembeli_id = $request['pembeli_id'];
        $hutang->save();

        toast('Data berhasil ditambahkan','success');
        return redirect('/hutang');
    }

    public function payment($id) 
    {
        $dataHutang = Hutang::find($id);
        return view('hutang.bayar', [
            'dataHutang' => $dataHutang
        ]);
    }

    public function bayar(Request $request, $id)
    {
        $dataHutang =  Hutang::find($id);
        $dataHutang->total_bayar += $request['total_bayar'];
        if(($dataHutang->total_hutang - $dataHutang->total_bayar) <= 0) {
            $dataHutang->status = 'Lunas';
        }
        $dataHutang->save();

        $pemasukan = new Pemasukan;
        $pemasukan->nominal = $request['total_bayar'];
        $pemasukan->keterangan = 'Bayar Hutang '. $dataHutang->pembeli->nama;
        $pemasukan->tanggal = date('Y/m/d',strtotime($request['tanggal']));
        $pemasukan->user_id = auth()->user()->id;
        $pemasukan->save();

        toast('Pembayaran Berhasil','success');
        return redirect('/hutang');
    }

    public function invoice($id) {
        $hutang = Hutang::findOrFail($id);
        $detailHutang = DetailHutang::where('hutang_id', $id)->get();
        $totalHutang = $detailHutang->sum('subtotal');
        $todayDate = Carbon::now()->format('d-m-Y');
        return view('hutang.invoice', [
            'hutang' => $hutang,
            'todayDate' => $todayDate,
            'detailHutang' => $detailHutang,
            'totalHutang' => $totalHutang
        ]);
    }

    public function download($id) {
        $hutang = Hutang::findOrFail($id);
        $detailHutang = DetailHutang::where('hutang_id', $id)->get();
        $totalHutang = $detailHutang->sum('subtotal');
        $todayDate = Carbon::now()->format('d-m-Y');
        $data = [
            'hutang' => $hutang,
            'todayDate' => $todayDate,
            'detailHutang' => $detailHutang,
            'totalHutang' => $totalHutang
        ];
        $pdf = Pdf::loadView('hutang.invoice', $data);
        return $pdf->download('invoice-'.$hutang->id . '-'. $todayDate.'.pdf');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
