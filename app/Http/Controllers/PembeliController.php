<?php

namespace App\Http\Controllers;

use App\Models\Pembeli;
use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;
use SebastianBergmann\CodeUnit\FunctionUnit;

class PembeliController extends Controller
{
    public function index() {
        $dataPembeli = Pembeli::latest()->get();
        return view('dataPembeli.index', compact('dataPembeli'));
    }
    public function create(){
        
        return view('hutang.tambahPengutang');
    }
    public function createFromMaster(){
        
        return view('dataPembeli.tambah');
    }

    public function store(Request $request) {
        $pembeli = new Pembeli;
        $pembeli->nama = $request['nama'];
        $pembeli->noTelp = $request['noTelp'];
        $pembeli->save();

        if($request['hal'] == "halamanMaster") {
            toast('Data berhasil ditambahkan','success');
            return redirect('/dataPembeli');
        } else {
            toast('Data berhasil ditambahkan','success');
            return redirect('/hutang/create');
        }
        
    }

    public function edit($id) {
        $data = Pembeli::find($id);
        return view('dataPembeli.edit', compact('data'));
    }

    public function update($id, Request $request) {
        $data = Pembeli::find($id);
        $data->nama = $request['nama'];
        $data->noTelp = $request['noTelp'];
        $data->save();

        toast('Data berhasil diedit','success');
        return redirect('/dataPembeli');
    }

    public function destroy($id) {
        $data = Pembeli::find($id);
        $data->delete();

        toast('Data berhasil dihapus','success');
        return redirect('/dataPembeli');
    }
}
