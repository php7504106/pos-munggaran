<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Hutang extends Model
{
    use HasFactory;
    protected $table='hutang';
    protected $guarded = ['id'];

    public function pembeli() {
        return $this->belongsTo(Pembeli::class);
    }

    public function detail_hutang() {
        return $this->hasMany(DetailHutang::class);
    }
}
