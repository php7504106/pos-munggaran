<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDetailHutangTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detail_hutang', function (Blueprint $table) {
            $table->id();
            $table->date('tanggal');
            $table->integer('nominal');
            $table->integer('qty');
            $table->integer('subtotal');
            $table->string('keterangan');
            $table->unsignedBigInteger('hutang_id');
            $table->foreign('hutang_id')->references('id')->on('hutang')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('detail_hutang');
    }
}
