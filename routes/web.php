<?php

use App\Http\Controllers\DashboardController;

use App\Models\Hutang;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\TransaksiHarian;
use App\Http\Controllers\HutangController;
use App\Http\Controllers\LogoutController;
use App\Http\Controllers\PembeliController;
use App\Http\Controllers\DetailHutangController;
use App\Http\Controllers\MenuController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/dashboard', [DashboardController::class, 'index'])->middleware('auth');
Route::get('/', function () {
    return view('auth.login');
});

Route::get('/transaksiHarian', function () {
    return view('transaksiHarian.index');
})->middleware('auth');

//Route Transaksi Harian
Route::get('/viewTransaksi', [TransaksiHarian::class, 'index'])->middleware('auth');
Route::get('/pemasukan/{pemasukan_id}', [TransaksiHarian::class, 'editPemasukan'])->middleware('auth');
Route::get('/pengeluaran/{pengeluaran_id}', [TransaksiHarian::class, 'editPengeluaran'])->middleware('auth');
Route::post('/tambahPendapatan', [TransaksiHarian::class, 'tambahPendapatan'])->middleware('auth');
Route::post('/tambahPengeluaran', [TransaksiHarian::class, 'tambahPengeluaran'])->middleware('auth');
Route::put('/updatePendapatan/{pemasukan_id}', [TransaksiHarian::class, 'updatePendapatan'])->middleware('auth');
Route::put('/updatePengeluaran/{pengeluaran_id}', [TransaksiHarian::class, 'updatePengeluaran'])->middleware('auth');
Route::delete('/hapusPemasukan/{pemasukan_id}', [TransaksiHarian::class, 'destroyPemasukan'])->middleware('auth');
Route::delete('/hapusPengeluaran/{pengeluaran_id}', [TransaksiHarian::class, 'destroyPengeluaran'])->middleware('auth');
//end route transaksi harian

//Route catatan hutang
Route::resource('hutang', HutangController::class);
Route::get('/tambahDataPengutang', [PembeliController::class, 'create']);
Route::post('/prosesTambahPengutang', [PembeliController::class, 'store']);
Route::get('/bayar/{hutang_id}', [HutangController::class, 'payment']);
Route::post('/prosesBayar/{hutang_id}', [HutangController::class, 'bayar']);
Route::get('/invoice/{hutang_id}', [HutangController::class, 'invoice']);
Route::get('/invoice/{hutang_id}/download', [HutangController::class, 'download']);

//detail hutang
Route::get('/tambahDetail', [DetailHutangController::class, 'index']);
Route::post('/prosesTambahDetail', [DetailHutangController::class, 'store']);
Route::get('/tambahDetailHutang/{id_hutang}', [DetailHutangController::class, 'create']);
Route::get('/editDetailHutang/{detailHutang_id}', [DetailHutangController::class, 'edit']);
Route::put('/prosesEditDetailHutang/{detailHutang_id}', [DetailHutangController::class, 'update']);
Route::delete('/hapusDetailHutang/{detailHutang_id}', [DetailHutangController::class, 'destroy']);
//end route catatan hutang

//data pembeli
Route::get('/dataPembeli', [PembeliController::class, 'index']);
Route::get('/pembeli/create', [PembeliController::class, 'createFromMaster']);
Route::get('/editPembeli/{pembeli_id}', [PembeliController::class, 'edit']);
Route::put('/prosesEditPembeli/{pembeli_id}', [PembeliController::class, 'update']);
Route::delete('/hapusPembeli/{pembeli_id}', [PembeliController::class, 'destroy']);

//data menu
Route::resource('menu', MenuController::class);

//logout
Route::post('/logout', [LogoutController::class, 'logout']);
Auth::routes();